<?php
/** 
 * As configurações básicas do WordPress.
 *
 * Esse arquivo contém as seguintes configurações: configurações de MySQL, Prefixo de Tabelas,
 * Chaves secretas, Idioma do WordPress, e ABSPATH. Você pode encontrar mais informações
 * visitando {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. Você pode obter as configurações de MySQL de seu servidor de hospedagem.
 *
 * Esse arquivo é usado pelo script ed criação wp-config.php durante a
 * instalação. Você não precisa usar o site, você pode apenas salvar esse arquivo
 * como "wp-config.php" e preencher os valores.
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar essas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */

define('WP_CACHE', true); //Added by WP-Cache Manager
define( 'WPCACHEHOME', '/srv/http/ismart/ismart/dev/wp-content/plugins/wp-super-cache/' ); //Added by WP-Cache Manager
if ($_SERVER['SERVER_ADDR'] == '192.168.0.120') {
	define('DB_NAME', 'ismart');
	/** Usuário do banco de dados MySQL */
	define('DB_USER', 'root');

	/** Senha do banco de dados MySQL */
	define('DB_PASSWORD', '');

	/** nome do host do MySQL */
	define('DB_HOST', 'localhost');
} else {
	define('DB_NAME', 'ismart');

	/** Usuário do banco de dados MySQL */
	define('DB_USER', 'adminEU5wWT2');

	/** Senha do banco de dados MySQL */
	define('DB_PASSWORD', '5XEJm22y2i6w');

	/** nome do host do MySQL */
	define('DB_HOST', getenv('OPENSHIFT_MYSQL_DB_HOST'));
}

/** Conjunto de caracteres do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8');

/** O tipo de collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Você pode alterá-las a qualquer momento para desvalidar quaisquer cookies existentes. Isto irá forçar todos os usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '-!(rcWne?[Zx$tp>ZvP$6!E}e!?L;N08/MJE^|?RkPUv.ggeg#-p}[#dsE{g,HK]');
define('SECURE_AUTH_KEY',  '8r!TL*ZG8yC%Nx;;ZSALZlr^aqGUf^P]au|#vSk%}U#ym}}b~Bq(Djz:$6JTi?l:');
define('LOGGED_IN_KEY',    '@v-y]A=F{ywh|}a;CRp?%b+tLg[(@1qH%.^m DbN`TeOe:~#ZRdZ(5Xn(U(dHP[z');
define('NONCE_KEY',        '-C1Gs0|8}<z1vZJ<KsS0~t+N$a8fi^j L&V^8MiQgCzP0*y4cI-|]{Ep2+lbKLqC');
define('AUTH_SALT',        '@+x5:+B=4Zw][:EJkQrNUq}w%j-/fU1m6uuz>2xi8VoW$3eYWTG|Qo5->_LKSUn;');
define('SECURE_AUTH_SALT', 'ja<~}/)P`<[|2^vA2]H$Q]<t0uV7K~`m|qg.rR)bNAGM~]uss2Ev[[C!zd(2,K^6');
define('LOGGED_IN_SALT',   'n?)ke:&+vH_SVe3e~Tr99B9Vk{0==ZnWpV[ sr|9*i(y84Jhl-a#Ztr(o]dBzlNU');
define('NONCE_SALT',       '8365R*:EJ|+u%A3DfdumI{xac m_4R`&cc3WkP7|hMp3_{,]NaWH@&xGyQu)HREE');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der para cada um um único
 * prefixo. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_';

/**
 * O idioma localizado do WordPress é o inglês por padrão.
 *
 * Altere esta definição para localizar o WordPress. Um arquivo MO correspondente ao
 * idioma escolhido deve ser instalado em wp-content/languages. Por exemplo, instale
 * pt_BR.mo em wp-content/languages e altere WPLANG para 'pt_BR' para habilitar o suporte
 * ao português do Brasil.
 */
define('WPLANG', 'pt_BR');

/**
 * Para desenvolvedores: Modo debugging WordPress.
 *
 * altere isto para true para ativar a exibição de avisos durante o desenvolvimento.
 * é altamente recomendável que os desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 */
define('WP_DEBUG', true);
define('FS_METHOD', 'direct');
define('WPCF7_LOAD_CSS', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');
	
/** Configura as variáveis do WordPress e arquivos inclusos. */
require_once(ABSPATH . 'wp-settings.php');
