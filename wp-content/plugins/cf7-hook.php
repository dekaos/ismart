<?php
/*
Plugin Name: Contact Form 7 Hook
Plugin URI: http://y2n.co
Description: Modifica a mensagem Mail (2)
Author: Henrique Furlan
Version: 1.0
Author URI: http://y2n.co
*/

function wpcf7_change_message ($WPCF7_ContactForm) {
	$submission = WPCF7_Submission::get_instance();
	$wpcf7      = WPCF7_ContactForm::get_current();
	
	if ($submission) {
		$data = $submission->get_posted_data();
		
		if(empty($data)) return;
	
		$motivo = isset($data['motivo']) ? $data['motivo'] : "";
		
		if( ($motivo == 'Processo Seletivo') ) {
			$mail1 = $wpcf7->prop('mail');

			$mail1['recipient'] = 'contatoismart@gmail.com';

	        $wpcf7->set_properties(array(
	            "mail" => $mail1
	        ));

			$mail2 = $wpcf7->prop('mail_2');
			//$mail2['subject'] = 'hudduiadaisuhsuiadh';
			$mail2['sender'] = 'Ismart <processoseletivo@ismart.org.br>';

			$mail2['subject'] = ':: Processo Seletivo Ismart ::';
			
			$mail2['body'] = nl2br("Olá! Tudo bem?\n\n Obrigado por seu interesse no Ismart, um instituto privado, sem fins lucrativos, que transforma jovens talentos em protagonistas do futuro.\n\n Recebemos sua dúvida a respeito do processo seletivo de bolsas de estudo.\n\n Os esclarecimentos que você precisa podem estar disponíveis na página do processo seletivo (http://www.ismart.org.br/processo-seletivo/).\n\n Consulte a seção Perguntas Frequentes e leia também os regulamentos antes de concorrer a uma vaga no Projeto Alicerce ou no Bolsa Talento.\n\n Em nosso blog, publicamos uma nota com várias informações sobre o Ismart e o processo seletivo 2015. Ela pode ser consultada neste link: http://www.ismart.org.br/2015/04/ismart-abre-inscricoes-para-bolsas-de-estudo-em-colegios-particulares-de-excelencia/.\n\n Se sua dificuldade for técnica, com o sistema da Primeira Escolha, parceira do Ismart na realização do processo seletivo, pedimos que envie um e-mail para suporte@primeiraescolha.com.br\n\n Se você é educador da rede pública e gostaria de indicar um aluno para o processo seletivo, orientamos que leia a página do Guia Prático para a Seleção do Ismart, um material feito para ajudar os professores: http://www.ismart.org.br/guia-educadores/.\n\n Se você trabalha em algum veículo de comunicação e gostaria de falar com o Ismart sobre o processo seletivo ou outros assuntos, por favor, envie um e-mail para comunicacao@ismart.org.br.\n\n Se após a leitura desta mensagem e de todos os links indicados a sua dúvida persistir, basta enviar uma resposta para este e-mail.\n\n Estamos à disposição.\n\n Atenciosamente,\n\n Equipe Ismart");
	        
	        $mail2['additional_headers'] = 'Reply-to: Ismart <processoseletivo@ismart.org.br>';
	        
	        $wpcf7->set_properties(array(
	            "mail_2" => $mail2
	        ));
	    }

        return $wpcf7;		
	}
	
}

add_action("wpcf7_before_send_mail", "wpcf7_change_message");

?>