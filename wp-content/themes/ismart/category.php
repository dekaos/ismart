<?php get_header(); ?>
    	<section id="feature" class="blog">
	    	<h1><span>BLOG</span></h1>
    	</section><!-- feature -->
		<section class="c clearfix">
		<section id="blog-content">
			<?php while(have_posts()) : the_post();  ?>
			<article class="post">
				<header><span class="data"><?php the_time('d F Y'); ?></span> • <span class="categoria"><?php $categories = get_the_category(); $separator = ', '; $output = ''; if($categories){ foreach($categories as $category) { $output .= '<a href="'.get_category_link( $category->term_id ).'" title="' . esc_attr( sprintf( __( "View all posts in %s" ), $category->name ) ) . '">'.$category->cat_name.'</a>'.$separator; } echo trim($output, $separator); } ?></span></header>
				<h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
				<?php if(has_post_thumbnail()) : ?>
				<a href="<?php the_permalink(); ?>"><img width="560" height="360" src="<?php echo get_post_image_url('size_560-360'); ?>"></a>
				<?php endif; ?>
				<?php the_excerpt(); ?>
				<a href="<?php the_permalink(); ?>" class="saiba-mais">Saiba mais</a>
			</article>
			<?php endwhile; ?>
			<?php wp_pagenavi(); ?>
		</section>

		<?php get_sidebar(); ?>

		</section>
<?php get_footer(); ?>