    	<section id="parallax">
	    	<h1>O Ismart acredita no talento individual e<br>no poder transformador da educação</h1>
    	</section>
    	
		<?php if(is_home()) { ?>    	
    	<section id="fale-conosco">
	    	<h1>Fale Conosco</h1>

	    	<div class="c clearfix">
<!--
	    		<div class="col1">
		    	<h2>São Paulo</h2>

				Rua Tabapuã, 500, cj. 64 - Itaim Bibi<br>
				CEP 04533-001<br>
				11 3078.6333
	    		</div>
	    		<div class="col2">
		    	<h2>Rio de Janeiro</h2>

				Av. Graça Aranha, 57, grupo 702 - Centro<br>
				CEP 20030-003<br>
				21 2224-0623
	    		</div>
-->
				    	<p>Entre em contato conosco. Estamos à disposição para receber dúvidas,<br>sugestões, pedidos de esclarecimentos e demandas da imprensa.</p>
			<?php echo do_shortcode('[contact-form-7 id="44" title="Fale Conosco"]'); ?>
	    	</div>
    	</section><!-- fale-conosco -->
    	
    	<?php } else { ?>
    	
     	<section id="newsletter">
	    	<h1>Newsletter</h1>
	    	<p>Cadastre-se para receber novidades sobre o Ismart.</p>
	    	<div class="c clearfix">
		    	<a href="#" class="newsletter-professores">Professores</a>
		    	<a href="#" class="newsletter-estudantes">Estudantes</a>
		    	<a href="#" class="newsletter-interessados">Interessados no Ismart</a>
	    	</div>
	    	<div id="professores" class="c clearfix">
				<?php 
					$widgetNL = new WYSIJA_NL_Widget(true);
					echo $widgetNL->widget(array('form' => 1, 'form_type' => 'php'));
				?>
	    	</div>
	    	
	    	<div id="estudantes" class="c clearfix">
				<?php 
					$widgetNL = new WYSIJA_NL_Widget(true);
					echo $widgetNL->widget(array('form' => 2, 'form_type' => 'php'));
				?>
	    	</div>
	    	
	    	<div id="interessados" class="c clearfix">
				<?php 
					$widgetNL = new WYSIJA_NL_Widget(true);
					echo $widgetNL->widget(array('form' => 3, 'form_type' => 'php'));
				?>
	    	</div>	    		    	
	    	
    	</section><!-- newsletter -->
    	
<!--    	<section id="enderecos">
    			<div class="c clearfix">
	    		<div class="col1">
		    	<h2>São Paulo</h2>

				Rua Tabapuã, 500, cj. 64 - Itaim Bibi<br>
				CEP 04533-001<br>
				11 3078.6333
	    		</div>
	    		<div class="col2">
		    	<h2>Rio de Janeiro</h2>

				Av. Graça Aranha, 57, grupo 702 - Centro<br>
				CEP 20030-003<br>
				21 2224-0623
	    		</div>
    			</div>
    	</section><!-- enderecos -->

		<?php } ?>
    	
        <footer id="footer">
	        <div class="c">
	        	<div class="icones"></div>
				<div class="rights">© 2014 Ismart. Todos os direitos reservados.</div>
				<div class="y2"><a href="http://y2n.co" target="_blank">Made with <span>♥</span> by y2&co</a></div>		        
	        </div>
        </footer><!-- footer -->
        
<!--
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="<?php echo get_template_directory_uri(); ?>/js/vendor/jquery-1.11.0.min.js"><\/script>')</script>
-->
		<script src="<?php echo get_template_directory_uri(); ?>/js/vendor/jquery.flexslider-min.js"></script>
		<script src="<?php echo get_template_directory_uri(); ?>/js/vendor/jquery.sharrre.min.js"></script>
		<script src="<?php echo get_template_directory_uri(); ?>/js/vendor/jquery.easydropdown.min.js"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/js/main.js"></script>
        <?php wp_footer(); ?>
        
		<div id="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=218837548295300&version=v2.0";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>  

		<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-51570602-1', 'ismart.org.br');
  ga('send', 'pageview');

</script>      
    </body>
</html>