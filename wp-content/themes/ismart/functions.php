<?php
//order numeric posts 
 function orderby_post_title_int( $orderby ) { return '(wp_posts.post_title+0) ASC'; }

 //order back-end posts in numeric order
 function set_custom_post_types_admin_order($wp_query) {
   if (is_admin()) {
   // Get the post type from the query  
     $post_type = $wp_query->query['post_type']; 
	 
   	  if ( $post_type == 'parceiro') { 
     	add_filter('posts_orderby', 'orderby_post_title_int' );
	  }
   }  
 }  
 add_filter('pre_get_posts', 'set_custom_post_types_admin_order');
 
 


// Register Theme Features
function custom_theme_features()  {

	// Add theme support for Featured Images
	add_theme_support( 'post-thumbnails' );	
}

add_image_size('size_480-480', 480, 480, true);
add_image_size('size_560-360', 560, 360, true);
add_image_size('size_960-480', 960, 480, true);
add_image_size('size_175-70' , 175, 70 , true);
add_image_size('size_180-150', 180, 150, true);
add_image_size('size_470-700', 470, 700, true);
add_image_size('size_460-400', 460, 400, true);
add_image_size('size_220-220', 220, 220, true);
add_image_size('size_170-254', 170, 254, true);

// Hook into the 'after_setup_theme' action
add_action( 'after_setup_theme', 'custom_theme_features' );

//============================================================================================================\\


function resized_image($image, $size) {

	$image_ext   = pathinfo($image, PATHINFO_EXTENSION);
	$image_parts = explode('.' . $image_ext, $image);
	$image_src   = $image_parts[0] . '-' . $size . '.' . $image_ext;

	if (@getimagesize($image_src)) {
		$image = $image_src;
	}

	return $image;
}

// Register Navigation Menus
function custom_navigation_menus() {

	$locations = array(
		'Header' => __( 'Header Menu', 'text_domain' ),
	);
	register_nav_menus( $locations );

}

// Hook into the 'init' action
add_action( 'init', 'custom_navigation_menus' );

//============================================================================================================\\

add_filter('sanitize_file_name', 'sa_sanitize_spanish_chars', 10);
function sa_sanitize_spanish_chars ($filename) {
	return remove_accents( $filename );
}

//============================================================================================================\\

function custom_excerpt_length( $length ) {
	return 19;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

//============================================================================================================\\

function new_excerpt_more( $more ) {
	return '';
}
add_filter('excerpt_more', 'new_excerpt_more');

//============================================================================================================\\

function get_post_image_url($size, $id = NULL) {
	$large_image_url = wp_get_attachment_image_src(get_post_thumbnail_id($id), $size);
	return $large_image_url[0];
}

//============================================================================================================\\

add_filter('acf/options_page/settings', 'my_options_page_settings');
function my_options_page_settings($options) {
	$options['title'] = __('Opções');
	$options['pages'] = array(__('Flickr'));
	return $options;
}

//============================================================================================================\\

add_filter( 'pre_get_posts' , 'search_exc_cats' );
function search_exc_cats( $query ) {
	if( $query->is_admin )
		return $query;
	if( $query->is_search ) {
		$query->set( 'post_type', array('post') );
		$query->set( 'category__not_in' , array( 8 ) );
	}
	return $query;
	}

//============================================================================================================\\
	
class email_return_path {
  	function __construct() {
		add_action( 'phpmailer_init', array( $this, 'fix' ) );    
  	}

	function fix( $phpmailer ) {
	  	$phpmailer->Sender = $phpmailer->From;
	}
}

new email_return_path();	



//============================================================================================================\\
//												CUSTOM POSTS
//============================================================================================================\\


// Register Custom Post Type
function custom_post_type_historia() {

	$labels = array(
		'name'                => _x( 'Histórias', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'História', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Histórias', 'text_domain' ),
		'parent_item_colon'   => __( 'História Pai:', 'text_domain' ),
		'all_items'           => __( 'Todas Histórias', 'text_domain' ),
		'view_item'           => __( 'Visualizar História', 'text_domain' ),
		'add_new_item'        => __( 'Adicionar Nova História', 'text_domain' ),
		'add_new'             => __( 'Adicionar Novo', 'text_domain' ),
		'edit_item'           => __( 'Editar História', 'text_domain' ),
		'update_item'         => __( 'Atualizar História', 'text_domain' ),
		'search_items'        => __( 'Buscar História', 'text_domain' ),
		'not_found'           => __( 'Nenhum encontrado', 'text_domain' ),
		'not_found_in_trash'  => __( 'Nenhum encontrado na lixeira', 'text_domain' ),
	);
	$args = array(
		'label'               => __( 'historia', 'text_domain' ),
		'description'         => __( 'Histórias de Sucesso', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail', ),
		//'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'menu_icon'           => '',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'historia', $args );

}

// Hook into the 'init' action
add_action( 'init', 'custom_post_type_historia', 0 );

//============================================================================================================\\

// Register Custom Post Type
function custom_post_type_parceiro() {

	$labels = array(
		'name'                => _x( 'Parceiros', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'Parceiro', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Parceiros', 'text_domain' ),
		'parent_item_colon'   => __( 'Parceiro Pai:', 'text_domain' ),
		'all_items'           => __( 'Todos Parceiros', 'text_domain' ),
		'view_item'           => __( 'Visualizar Parceiro', 'text_domain' ),
		'add_new_item'        => __( 'Adicionar Novo Parceiro', 'text_domain' ),
		'add_new'             => __( 'Adicionar Novo', 'text_domain' ),
		'edit_item'           => __( 'Editar Parceiro', 'text_domain' ),
		'update_item'         => __( 'Atualizar Parceiro', 'text_domain' ),
		'search_items'        => __( 'Buscar Parceiro', 'text_domain' ),
		'not_found'           => __( 'Nenhum encontrado', 'text_domain' ),
		'not_found_in_trash'  => __( 'Nenhum encontrado na lixeira', 'text_domain' ),
	);
	$args = array(
		'label'               => __( 'parceiro', 'text_domain' ),
		'description'         => __( 'Parceiros', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'thumbnail', ),
		'taxonomies'          => array( 'tipo_parceiro' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'menu_icon'           => '',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'parceiro', $args );

}

// Hook into the 'init' action
add_action( 'init', 'custom_post_type_parceiro', 0 );

//============================================================================================================\\

// Register Custom Post Type
function custom_post_type_marca() {

	$labels = array(
		'name'                => _x( 'Marcas', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'Marca', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Deixe sua Marca', 'text_domain' ),
		'parent_item_colon'   => __( 'Marca Pai:', 'text_domain' ),
		'all_items'           => __( 'Todas Marcas', 'text_domain' ),
		'view_item'           => __( 'Visualizar Marca', 'text_domain' ),
		'add_new_item'        => __( 'Adicionar Novo Marcas', 'text_domain' ),
		'add_new'             => __( 'Adicionar Nova', 'text_domain' ),
		'edit_item'           => __( 'Editar Marca', 'text_domain' ),
		'update_item'         => __( 'Atualizar Marca', 'text_domain' ),
		'search_items'        => __( 'Buscar Marca', 'text_domain' ),
		'not_found'           => __( 'Nenhum encontrado', 'text_domain' ),
		'not_found_in_trash'  => __( 'Nenhum encontrado na lixeira', 'text_domain' ),
	);
	$args = array(
		'label'               => __( 'marca', 'text_domain' ),
		'description'         => __( 'Deixe Sua Marca', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail', ),
		'taxonomies'          => array( 'tipo', 'ano' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'menu_icon'           => '',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'marca', $args );

}

// Hook into the 'init' action
add_action( 'init', 'custom_post_type_marca', 0 );

//============================================================================================================\\

// Register Custom Post Type
function custom_post_type_relatorio() {

	$labels = array(
		'name'                => _x( 'Relatórios de Atividades', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'Relatório de Atividade', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Relatório de Atividade', 'text_domain' ),
		'parent_item_colon'   => __( 'Relatório Pai:', 'text_domain' ),
		'all_items'           => __( 'Todos Relatórios', 'text_domain' ),
		'view_item'           => __( 'Visualizar Relatório', 'text_domain' ),
		'add_new_item'        => __( 'Adicionar Novo Relatório', 'text_domain' ),
		'add_new'             => __( 'Adicionar Novo', 'text_domain' ),
		'edit_item'           => __( 'Editar Relatório', 'text_domain' ),
		'update_item'         => __( 'Atualizar Relatório', 'text_domain' ),
		'search_items'        => __( 'Buscar Relatório', 'text_domain' ),
		'not_found'           => __( 'Nenhum encontrado', 'text_domain' ),
		'not_found_in_trash'  => __( 'Nenhum encontrado na lixeira', 'text_domain' ),
	);
	$args = array(
		'label'               => __( 'relatorio', 'text_domain' ),
		'description'         => __( 'Relatórios de Atividade', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'thumbnail', ),
		//'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'menu_icon'           => '',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'relatorio', $args );

}

// Hook into the 'init' action
add_action( 'init', 'custom_post_type_relatorio', 0 );

//============================================================================================================\\

// Register Custom Post Type
function custom_post_type_pergunta() {

	$labels = array(
		'name'                => _x( 'Perguntas Frequentes', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'Pergunta Frequente', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Perguntas Frequentes', 'text_domain' ),
		'parent_item_colon'   => __( 'Pergunda Pai:', 'text_domain' ),
		'all_items'           => __( 'Todas Perguntas', 'text_domain' ),
		'view_item'           => __( 'Visualizar Pergunta', 'text_domain' ),
		'add_new_item'        => __( 'Adicionar Nova Pergunta', 'text_domain' ),
		'add_new'             => __( 'Adicionar Nova', 'text_domain' ),
		'edit_item'           => __( 'Editar Pergunta', 'text_domain' ),
		'update_item'         => __( 'Atualizar Pergunta', 'text_domain' ),
		'search_items'        => __( 'Buscar Pergunta', 'text_domain' ),
		'not_found'           => __( 'Nenhuma encontrada', 'text_domain' ),
		'not_found_in_trash'  => __( 'Nenhuma encontrada na lixeira', 'text_domain' ),
	);
	$args = array(
		'label'               => __( 'pergunta', 'text_domain' ),
		'description'         => __( 'Perguntas Frequentes', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', ),
		//'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'menu_icon'           => '',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'pergunta', $args );

}

// Hook into the 'init' action
add_action( 'init', 'custom_post_type_pergunta', 0 );

//============================================================================================================\\
//												CUSTOM TAXONOMY
//============================================================================================================\\

// Register Custom Taxonomy
function custom_taxonomy_tipo_parceiro() {

	$labels = array(
		'name'                       => _x( 'Tipos de Parceiros', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              => _x( 'Tipo de Parceiro', 'Taxonomy Singular Name', 'text_domain' ),
		'menu_name'                  => __( 'Tipo de Parceiro', 'text_domain' ),
		'all_items'                  => __( 'Todos os Tipos', 'text_domain' ),
		'parent_item'                => __( 'Tipo Pai', 'text_domain' ),
		'parent_item_colon'          => __( 'Tipo Pai:', 'text_domain' ),
		'new_item_name'              => __( 'Nome do Novo Tipo', 'text_domain' ),
		'add_new_item'               => __( 'Adicionar Novo Tipo', 'text_domain' ),
		'edit_item'                  => __( 'Editar Tipo', 'text_domain' ),
		'update_item'                => __( 'Atualizar Tipo', 'text_domain' ),
		'separate_items_with_commas' => __( 'Tipos separados por vírgulas', 'text_domain' ),
		'search_items'               => __( 'Buscar Tipos', 'text_domain' ),
		'add_or_remove_items'        => __( 'Adicionar ou remover Tipos', 'text_domain' ),
		'choose_from_most_used'      => __( 'Escolha dos Tipos mais utilizados', 'text_domain' ),
		'not_found'                  => __( 'Não encontrado', 'text_domain' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'tipo_parceiro', array( 'parceiro' ), $args );

}

// Hook into the 'init' action
add_action( 'init', 'custom_taxonomy_tipo_parceiro', 0 );

//============================================================================================================\\

// Register Custom Taxonomy
function custom_taxonomy_tipo() {

	$labels = array(
		'name'                       => _x( 'Tipos', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              => _x( 'Tipo', 'Taxonomy Singular Name', 'text_domain' ),
		'menu_name'                  => __( 'Tipo', 'text_domain' ),
		'all_items'                  => __( 'Todos Tipos', 'text_domain' ),
		'parent_item'                => __( 'Tipo Pai', 'text_domain' ),
		'parent_item_colon'          => __( 'Tipo Pai:', 'text_domain' ),
		'new_item_name'              => __( 'Nome do Novo Tipo', 'text_domain' ),
		'add_new_item'               => __( 'Adicionar Novo TIpo', 'text_domain' ),
		'edit_item'                  => __( 'Editar Tipo', 'text_domain' ),
		'update_item'                => __( 'Atualizar Tipo', 'text_domain' ),
		'separate_items_with_commas' => __( 'Tipos separados por vírgulas', 'text_domain' ),
		'search_items'               => __( 'Buscar Tipos', 'text_domain' ),
		'add_or_remove_items'        => __( 'Adicionar ou remover Tipos', 'text_domain' ),
		'choose_from_most_used'      => __( 'Escolha dos tipos mais usados', 'text_domain' ),
		'not_found'                  => __( 'Não encontrado', 'text_domain' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'tipo', array( 'marca' ), $args );

}

// Hook into the 'init' action
add_action( 'init', 'custom_taxonomy_tipo', 0 );

//============================================================================================================\\

// Register Custom Taxonomy
function custom_taxonomy_ano() {

	$labels = array(
		'name'                       => _x( 'Anos', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              => _x( 'Ano', 'Taxonomy Singular Name', 'text_domain' ),
		'menu_name'                  => __( 'Ano', 'text_domain' ),
		'all_items'                  => __( 'Todos Anos', 'text_domain' ),
		'parent_item'                => __( 'Ano Pai', 'text_domain' ),
		'parent_item_colon'          => __( 'Ano Pai:', 'text_domain' ),
		'new_item_name'              => __( 'Nome do Novo Ano', 'text_domain' ),
		'add_new_item'               => __( 'Adicionar Novo Ano', 'text_domain' ),
		'edit_item'                  => __( 'Editar Ano', 'text_domain' ),
		'update_item'                => __( 'Atualizar Ano', 'text_domain' ),
		'separate_items_with_commas' => __( 'Anos separados por vírgulas', 'text_domain' ),
		'search_items'               => __( 'Buscar Anos', 'text_domain' ),
		'add_or_remove_items'        => __( 'Adicionar ou remover anos', 'text_domain' ),
		'choose_from_most_used'      => __( 'Escolha dos anos mais usados', 'text_domain' ),
		'not_found'                  => __( 'Não encontrado', 'text_domain' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'ano', array( 'marca' ), $args );

}

//Custom WordPress Login Logo 
function cutom_login_logo() {
echo "<style type=\"text/css\">
body.login div#login h1 a {
background-image: url(".get_bloginfo('template_directory')."/img/ismart.jpg);
padding-bottom: 10px;
}
</style>";
}
add_action( 'login_enqueue_scripts', 'cutom_login_logo' );


// Hook into the 'init' action
add_action( 'init', 'custom_taxonomy_ano', 0 );
