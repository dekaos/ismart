<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title><?php bloginfo('name'); ?> | <?php is_home() ? bloginfo('description') : wp_title(''); ?></title>
        <meta name="description" content="<?php bloginfo('description'); ?>">
		<meta name="format-detection" content="telephone=no">
		<link rel="shortcut icon" href="<?php bloginfo('url'); ?>/favicon.ico"/>
		<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/normalize.min.css">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/flexslider.css">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/easydropdown.flat.css">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/main.css">
        <!--[if lt IE 9]>
            <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
            <script>window.html5 || document.write('<script src="<?php echo get_template_directory_uri(); ?>/js/vendor/html5shiv.js"><\/script>')</script>
        <![endif]-->
        <?php wp_head(); ?>
    </head>
    <body>
    	<section class="strip_big"></section>
    	
    	<header id="header">
	    	<?php wp_nav_menu(array('menu' => 'Principal', 'container' => 'nav', 'menu_class' => 'c clearfix')); ?>
    	</header>