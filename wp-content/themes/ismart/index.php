<?php get_header(); ?>
    	<section id="feature">
			<?php echo do_shortcode('[rev_slider home]'); ?>
    	</section>
    	<section class="full_col clearfix">
	    	<section id="talentos">
				<h1><span>O Ismart</span><br>transforma talentos<br><span>em protagonistas</span><br>do futuro</h1>
	    	</section><!-- talentos -->
	    	
	    	<section id="processo-seletivo" style="background: url(http://www.ismart.org.br/wp-content/uploads/2015/03/bg_processo_seletivo.jpg);">
				<h1><span class="processo">Processo</span><br>Seletivo<br><span class="ano">2015</span></h1>
				<h2>Inscrições abertas<br>até 12 de junho!</h2>
				<p>Alunos de <strong>SP e Rio</strong> podem<br>concorrer às bolsas de estudo.</p>
				<a href="<?php bloginfo('url'); ?>/?page_id=133" class="inscreva-se">Saiba mais</a>
	    	</section><!-- processo-seletivo -->
			<?php $args = array('post_type' => 'historia', 'posts_per_page' => 1, 'orderby' => 'rand'); $historia = new WP_Query($args); ?>
			<?php if($historia->have_posts()) : $historia->the_post(); ?>
			<section id="historias-picture" style="background: url(<?php echo get_post_image_url('full'); ?>) no-repeat center top;">
			</section> <!-- historias-picture -->
			
			<section id="historias-sucesso">
				<h1><span>Histórias de</span> Sucesso</h1>
				<h2><?php the_title(); ?></h2>
				<h3><?php $cat = get_category(get_field('his_projeto')); echo $cat->name; ?></h3>
				<?php the_content(); ?>				
				<p class="conheca-mais">Conheça mais histórias que<br>foram transformadas</p>
				<a href="<?php bloginfo('url'); ?>/?page_id=121" class="conheca-mais"></a>
			<section id="historias-bg"></section><!-- historias-bg -->
			</section> <!-- historias-sucesso -->
			
			<?php endif; ?>

			<section id="fala">
	        	<a href="http://www.youtube.com/embed/g0W8OHJNXjI?autoplay=1" class="fancybox iframe"><span class="icon"></span><h1>Alunos estudam pela internet<br>no projeto Ismart Online</h1></a>
	        	<img src="<?php echo get_template_directory_uri(); ?>/img/video_home.jpg">
			</section><!-- fala -->

			<?php $args = array('post_type' => 'post', 'posts_per_page' => 1, 'category_name' => 'blog'); $blog1 = new WP_Query($args); ?>
			<?php if($blog1->have_posts()) : while($blog1->have_posts()) : $blog1->the_post(); ?>			
			<section id="blog_1" class="clearfix">
				<div class="container">
					<h1>Blog</h1>
					<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
					<span class="data"><?php the_time('d F Y'); ?></span>				
				</div>
				<div class="mask"></div>
				<img width="480" height="480" src="<?php echo get_post_image_url('size_480-480'); ?>">
			</section><!-- blog1 -->
			<?php endwhile; endif; ?>

			<section id="facebook">
				<a href="https://www.facebook.com/ismart.oficial" target="_blank">
					<i class="fa fa-facebook"></i>
					<h1>Facebook</h1>
					<h2>Curta nossa página</h2>
				</a>
			</section><!-- facebook -->
			<?php $args = array('post_type' => 'post', 'posts_per_page' => 1, 'offset' => 1, 'category_name' => 'blog'); $blog2 = new WP_Query($args); ?>
			<?php if($blog2->have_posts()) : while($blog2->have_posts()) : $blog2->the_post(); ?>
			<section id="blog_2">
					<h1>Blog</h1>
					<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
					<span class="data"><?php the_time('d F Y'); ?></span>				
			</section><!-- blog2 -->
			<?php endwhile; endif; ?>

			<section id="flickr">
				<a href="https://www.flickr.com/photos/ismartoficial/sets" target="_blank">
					<div class="bg"><i class="fa fa-flickr"></i></div>
					<h1>Flickr</h1>
					<h2>Veja nossa galeria de fotos</h2>
				</a>				
			</section><!-- flickr -->
			
			<section id="flickr-picture">
				<?php

				?>
				<a href="https://www.flickr.com/photos/ismartoficial/sets" target="_blank"><img width="960" height="480" src="<?php echo resized_image(get_field('fli_foto', 'options'), '960x480'); ?>"></a>
			</section><!-- flickr-picture -->			
    	</section><!-- full -->
    	
    	<section id="parceiros">
	    	<h1><span>Nossos</span> Parceiros</h1>
	    	<h2>O ISMART conta com o apoio e o investimento de um seleto grupo de parceiros. Sem o suporte, não seria possível construir essa história de sucesso.</h2>
	    	<div id="parceiros-slider" class="flexslider clearfix">
	    		<ul class="slides">
					<?php $terms = get_terms('tipo_parceiro', 'hide_empty=1&orderby=slug'); foreach($terms as $term) { ?>
					<?php $args = array('post_type' => 'parceiro', 'posts_per_page' => -1, 'orderby' => 'title', 'order' => 'ASC', 'tax_query' => array(array('taxonomy' => 'tipo_parceiro', 'field' => 'term_id', 'terms' => $term->term_id))); add_filter('posts_orderby', 'orderby_post_title_int' ); $parceiro = new WP_Query($args); ?>
					<li>
						<h2><?php echo $term->name; ?></h2>
						<?php while($parceiro->have_posts()) : $parceiro->the_post(); ?>
						<?php if(get_field('par_link')) { ?>
							<a href="<?php the_field('par_link'); ?>" target="_blank"><img src="<?php echo get_post_image_url('size_175-70'); ?>" alt="<?php the_title(); ?>" width="175" height="70"></a>
						<?php } else { ?>
							<a href="#"><img src="<?php echo get_post_image_url('size_175-70'); ?>" alt="<?php the_title(); ?>" width="175" height="70"></a>
						<?php } ?>
						<?php endwhile; ?>
					</li>
					<?php } ?>
				</ul>
	    	</div>
<!-- 	    	<a href="#" class="veja-mais">Veja mais</a> -->
    	</section><!-- parceiros -->
    	
    	<section id="imprensa">
	    	<header id="header-imprensa">
		    	<h1>Imprensa</h1>
	    	</header>
	    	<?php $args = array('post_type' => 'post', 'posts_per_page' => -1, 'category_name' => 'imprensa'); $imprensa = new WP_Query($args); ?>
	    	<?php if($imprensa->have_posts()) : ?>
	    	<section id="notebook-slider" class="flexslider">
		    	<ul class="slides">
			    	<?php while($imprensa->have_posts()) : $imprensa->the_post(); ?>
			    	<li>
				    	<img src="<?php echo get_post_image_url('size_180-150'); ?>" width="180" height="150">
				    	<h1><?php the_title(); ?></h1>
				    	<p><?php the_excerpt(); ?></p>
				    	<?php if(get_field('imp_link')) : ?><a href="<?php the_field('imp_link'); ?>" target="_blank">Saiba mais</a><?php endif; ?>
			    	</li>
			    	<?php endwhile; ?>
		    	</ul>
	    	</section>
	    	<?php endif; ?>
    	</section><!-- imprensa -->
    	
    	
    	<section class="full_col clearfix">
	    	<section id="deixe-sua-marca">
		    	<div class="container">
			    	<p>Os professores que já tiveram alunos aprovados no Processo Seletivo do Ismart <span>merecem nosso reconhecimento.</span></p>
					<a href="<?php bloginfo('url'); ?>/?page_id=130" class="saiba-mais">Saiba mais</a>
		    	</div>
	    	</section><!-- deixe sua marca -->
	    	
	    	<section id="liga-do-futuro">
		    	<div class="container">
			    	<p><span>Novo programa do Ismart</span> criado para incentivar os alunos a desenvolver habilidades como motivação, autonomia, engajamento e atitude.</p>
					<a href="http://ligadofuturo.ismart.org.br" target="_blank" class="saiba-mais">Saiba mais</a>
		    	</div>		    	
	    	</section><!-- liga do futuro -->
    	</section><!-- full -->
<?php get_footer(); ?>