(function($){
	$(window).load(function() {
		$('#parceiros-slider').flexslider({
			directionNav: false,
			controlNav: true,
			smoothHeight: false,
		});
		
		$('#notebook-slider').flexslider({
			directionNav: true,
			controlNav: true,
		});
	});
	
	$(document).ready(function(){
		$('li.missao a').click(function(e){
			e.preventDefault();
			$('#missao').css({zIndex : 9});
			$('#visao, #valores').css({zIndex : 8});
			$('#missao-visao-valores ul').find('li.ativo').removeClass('ativo');
			$(this).parent().addClass('ativo');
		});
		$('li.visao a').click(function(e){
			e.preventDefault();
			$('#visao').css({zIndex : 9});
			$('#missao, #valores').css({zIndex : 8});
			$('#missao-visao-valores ul').find('li.ativo').removeClass('ativo');
			$(this).parent().addClass('ativo');
		});
		$('li.valores a').click(function(e){
			e.preventDefault();
			$('#valores').css({zIndex : 9});
			$('#missao, #visao').css({zIndex : 8});
			$('#missao-visao-valores ul').find('li.ativo').removeClass('ativo');
			$(this).parent().addClass('ativo');
		});
		
		$('.newsletter-professores').click(function(e){
			e.preventDefault();
			$(this).parent().slideUp();
			$('#professores').slideDown();	
		});
		
		$('.newsletter-estudantes').click(function(e){
			e.preventDefault();
			$(this).parent().slideUp();
			$('#estudantes').slideDown();	
		});		

		$('.newsletter-interessados').click(function(e){
			e.preventDefault();
			$(this).parent().slideUp();
			$('#interessados').slideDown();	
		});

		$('#btn-sp').click(function(e){
			e.preventDefault();
			$(this).parent().slideUp();
			$('#form-sp').slideDown();	
		});

		$('#btn-rj').click(function(e){
			e.preventDefault();
			$(this).parent().slideUp();
			$('#form-rj').slideDown();	
		});
		
		$('.anos a').click(function(e){
			e.preventDefault();
			$('.anos').find('li.ativo').removeClass('ativo');
			$(this).parent().addClass('ativo');
			var ano = $(this).attr('data-ano');
			console.log(ano);
			$('ul.ano').hide();
			$('ul.ano.'+ano).show();
		});
		
		$('.ano li').hover(function(){
			$(this).find('.texto').fadeIn();
		}, function(){
			$(this).find('.texto').fadeOut();
		});
			
				
		$('#enviar').click(function(e){
			$(this).html('<i class="fa fa-cog fa-spin"></i>');
			e.preventDefault();
			$.ajax({
				type: 'post',
				url: $('#contactform').attr('action'),
				data: $('#contactform').serialize(),
				success: function(data) {
					$('#enviar').html('<span style="color: green"><i class="fa fa-thumbs-up"></i> Enviado</span>');
				},
				error: function(data){
					$('#enviar').html('<span style="color: red"><i class="fa fa-thumbs-down"></i> Erro</span>');
				}
			});
		});
		
		$('.wysija-select ').easyDropDown(); 
		
		$('#perguntas-frequentes dt > a').click(function() {
			if($(this).parent().hasClass('closed')) {
				$(this).parent().next().stop(true, true).slideDown();
				$(this).parent().addClass('opened').removeClass('closed');
			}
			else if($(this).parent().hasClass('opened')) {
				$(this).parent().next().stop(true, true).slideUp();
				$(this).parent().addClass('closed').removeClass('opened');			
			}
			return false;
		});
		
		$('.default-value').each(function() {
		
		   var default_value = $(this).val();
		
		   $(this).focus(function(){
		           if($(this).val() == default_value) {
		                   $(this).val('');
		           }
		   });
		
		   $(this).blur(function(){
		           if($(this).val() == '') {
		                   $(this).val(default_value);
		           }
		   });
		   
		});
		
		$('#shareme').sharrre({
		  share: {
		    googlePlus: true,
		    facebook: true,
		    twitter: true,
		    linkedin: true,
		    pinterest: true
		  },
		  buttons: {
		    googlePlus: {size: 'standard', annotation:'bubble'},
		    facebook: {layout: 'button_count', send: 'true', },
		    twitter: {count: 'horizontal'},
		    linkedin: {counter: 'right'},
		    pinterest: { layout: 'horizontal'}
		  },
		  enableHover: false,
		  enableCounter: false,
		  enableTracking: true
		});				
		
	});	
})(jQuery);