<?php get_header(); ?>
    	<section id="feature" class="projetos alicerce">
	    	<h1><span>Projeto</span> Alicerce</h1>
	    	<p>Os bolsistas do <strong>Projeto Alicerce</strong> continuam matriculados em suas escolas de origem. <strong>No contraturno, frequentam um curso preparatório de dois anos de duração</strong> em um dos colégios particulares parceiros do Ismart em São Paulo, São José dos Campos e Rio de Janeiro.</p>
    	</section><!-- feature -->
    	
    	
    	<section id="projetos" class="c">
	    	<p class="laranja">O Ismart fornece aos bolsistas<br>material didático, uniforme, transporte e alimentação necessários<br>para eles frequentarem os colégios parceiros.</p>
	    	<p class="divisor laranja"></p>
			<p><strong>O curso tem como objetivo preparar os alunos para o exame de seleção para o ensino médio na<br> escola particular.</strong> Quem consegue bom desempenho, tem boa disciplina e é aprovado no exame de<br> seleção continua como bolsista do Ismart, que passa a pagar as mensalidades das escolas privadas.</p>
			<p>Podem se candidatar ao Projeto Alicerce os <strong>alunos matriculados no 7º ano (antiga 6ª série) do<br> ensino fundamental</strong> em escolas públicas ou particulares, desde que vindos de famílias com a renda<br> máxima estipulada pelo Ismart.</p>
			<p><strong>Para participar do processo seletivo para o Projeto Alicerce, o estudante deve obrigatoriamente<br> atender aos seguintes critérios:</strong></p>		
			
			<section id="projeto-container" class="clearfix">
					<ul id="criterios" class=" clearfix laranja">
						<li>
							<span class="numero">1</span>
							Estar cursando o 7º ano do ensino fundamental e ter faixa etária adequada à série;							
						</li>
						<li>
							<span class="numero">2</span>
							Nunca ter repetido o ano escolar;							
						</li>
						<li>
							<span class="numero">3</span>
							Ser proveniente de família com renda per capita (ou renda por pessoa) familiar de no máximo dois salários mínimos.							
						</li>					
					</ul>
					<!--
					<section id="inscricoes-abertas" class="clearfix">
					<h1 class="inscricoes laranja">Já estão abertas <strong>as inscrições.</strong></h1>
					<a href="http://www.ismart.org.br/?page_id=133" class="inscreva-se single laranja">Inscreva-se</a>
					</section>
					-->

					<?php if(have_rows('rf_escolas_parceiras')) : ?>
					<section id="escolas-parceiras" class="clearfix">
						<h1><span>Escolas Parceiras do</span> Projeto Alicerce</h1>
						<?php while(have_rows('rf_escolas_parceiras')) : the_row(); ?>
						<ul class="escolas clearfix">							
							<h2><?php the_sub_field('es_cidade'); ?></h2>
							<?php while(have_rows('rf_escolas')) : the_row(); ?>
							<li><?php if(get_sub_field('es_link')) : ?><a href="<?php the_sub_field('es_link'); ?>"> <?php endif; ?><img src="<?php echo resized_image(get_sub_field('es_imagem'), '175x70'); ?>" alt="<?php the_title(); ?>" width="175" height="70"><?php if(get_sub_field('es_link')) : ?></a><?php endif; ?></li>
							<?php endwhile; ?>
						</ul>
						<?php endwhile; ?>
					</section>
					<?php endif; ?>
			</section>			
		</section><!-- projetos -->
	
<?php get_footer(); ?>