<?php get_header(); ?>
    	<section id="feature" class="projetos bolsa-talento">
	    	<h1><span>Projeto</span> Bolsa Talento</h1>
	    	<p>Podem se candidatar ao Projeto Bolsa Talento os <strong>alunos matriculados<br> no 9º ano (antiga 8ª série) do ensino fundamental</strong> em escolas<br> públicas ou particulares, desde que vindos de famílias com a renda<br> máxima estipulada pelo Ismart.</p>
    	</section><!-- feature -->
    	
    	
    	<section id="projetos" class="c">
	    	<p class="roxo">Os alunos aprovados para o Projeto Bolsa Talento 
ingressam no 1º ano do ensino médio em uma escola particular parceira do Ismart com <span>bolsa de estudos integral que inclui mensalidade, material escolar, uniforme e cobertura de despesas com transporte e alimentação.</span></p>
	    	<p class="divisor roxo"></p>
			<p>Os colégios parceiros do Ismart no Projeto Bolsa Talento ficam em São Paulo (capital, Cotia e Sorocaba)<br> e no Rio de Janeiro.</p>
			<p><strong>Para participar do processo seletivo para o Projeto Bolsa Talento, o estudante deve<br> obrigatoriamente atender aos seguintes critérios:</strong></p>
			
			<section id="projeto-container" class="clearfix">

					<ul id="criterios" class=" clearfix roxo">
						<li>
							<span class="numero">1</span>
							Estar cursando o 9º ano do ensino fundamental e ter faixa etária adequada à série;							
						</li>
						<li>
							<span class="numero">2</span>
							Nunca ter repetido o ano escolar;							
						</li>
						<li>
							<span class="numero">3</span>
							Ser proveniente de família com renda per capita (ou renda por pessoa) familiar de no máximo dois salários mínimos.							
						</li>					
					</ul>
					<!--
					<section id="inscricoes-abertas" class="clearfix">
					<h1 class="inscricoes roxo">Já estão abertas <strong>as inscrições.</strong></h1>
					<a href="http://www.ismart.org.br/?page_id=133" class="inscreva-se single roxo">Inscreva-se</a>
					</section>
					-->


					<?php if(have_rows('rf_escolas_parceiras')) : ?>
					<section id="escolas-parceiras" class="clearfix">
						<h1><span>Escolas Parceiras do</span> Projeto Bolsa Talento</h1>
						<?php while(have_rows('rf_escolas_parceiras')) : the_row(); ?>
						<ul class="escolas clearfix">							
							<h2><?php the_sub_field('es_cidade'); ?></h2>
							<?php while(have_rows('rf_escolas')) : the_row(); ?>
							<li><?php if(get_sub_field('es_link')) : ?><a href="<?php the_sub_field('es_link'); ?>"> <?php endif; ?><img src="<?php echo resized_image(get_sub_field('es_imagem'), '175x70'); ?>" alt="<?php the_title(); ?>" width="175" height="70"><?php if(get_sub_field('es_link')) : ?></a><?php endif; ?></li>
							<?php endwhile; ?>
						</ul>
						<?php endwhile; ?>
					</section>
					<?php endif; ?>
			</section>
		</section><!-- projetos -->
	
<?php get_footer(); ?>