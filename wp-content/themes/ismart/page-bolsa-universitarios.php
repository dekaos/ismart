<?php get_header(); ?>
    	<section id="feature" class="projetos bolsa-universitarios">
	    	<h1><span>Projeto</span> Bolsa Universitários</h1>
	    	<p>A bolsa-auxílio para universitários se destina exclusivamente a alunos que são bolsistas do Ismart ao fim do ensino médio e ingressam no ensino superior em cursos apoiados pelo instituto.</p>
    	</section><!-- feature -->
    	
    	
    	<section id="projetos" class="c">
	    	<p class="verde">Os bolsistas aprovados no vestibular segundo os critérios de<br> aceitação do Ismart passam a receber, durante o curso superior,<br> uma bolsa-auxílio no valor de um salário mínimo mensal,<br> durante o período da faculdade.</p>
	    	<p class="divisor verde"></p>
			<p>Os alunos também têm acesso a uma série de atividades que os ajudam a chegar mais preparados<br> para o mercado de trabalho, como programas de mentoria e coaching e oficinas para<br> desenvolvimento de habilidades de comunicação.</p>
			
			<section id="projeto-container" class="clearfix">
					<?php if(have_rows('rf_escolas_parceiras')) : ?>
					<section id="escolas-parceiras" class="clearfix">
						<h1><span>Escolas Parceiras do</span> Projeto Alicerce</h1>
						<?php while(have_rows('rf_escolas_parceiras')) : the_row(); ?>
						<ul class="escolas clearfix">							
							<h2><?php the_sub_field('es_cidade'); ?></h2>
							<?php while(have_rows('rf_escolas')) : the_row(); ?>
							<li><?php if(get_sub_field('es_link')) : ?><a href="<?php the_sub_field('es_link'); ?>"> <?php endif; ?><img src="<?php echo resized_image(get_sub_field('es_imagem'), '175x70'); ?>" alt="<?php the_title(); ?>" width="175" height="70"><?php if(get_sub_field('es_link')) : ?></a><?php endif; ?></li>
							<?php endwhile; ?>
						</ul>
						<?php endwhile; ?>
					</section>
					<?php endif; ?>
			</section>
		</section><!-- projetos -->
	
<?php get_footer(); ?>