<?php get_header(); ?>
    	<section id="feature" class="projetos marca">
	    	<h1><span>Deixe sua Marca</span></h1>
	    	<p>O Deixe sua Marca é o novo <strong>programa de relacionamento do<br> Ismart</strong> com educadores da rede pública.</p>
    	</section><!-- feature -->
    	
    	
    	<section id="projetos" class="c">
	    	<div id="deixa-sua-marca-logo"></div>
	    	<p class="azul">Para participar, o professor deve participar do Encontro<br> Ismart com Educadores da Rede Pública ou inscrever alunos<br> para o processo seletivo do Ismart.</p>
	    	<p class="divisor azul"></p>
			<p>Aqui no site nós vamos contar histórias de professores que acreditam na missão do Ismart, apoiam a<br> divulgação do projeto nas escolas públicas e indicam alunos para o processo seletivo de bolsas de estudos.</p>
			<p>Se você é professor de escola pública, <strong>deixe sua marca</strong> indicando um aluno para o processo seletivo do<br> Ismart!</p>
			
			<section id="projeto-container" class="clearfix">
				<div id="fazendo-a-diferenca" class="deixe-sua-marca">
					<h2><span>Fazendo a</span> Diferença</h2>
					<p>Os educadores que tiveram <strong>alunos aprovados</strong> no processo seletivo do Ismart falam de sua relação com os estudantes e contam como identificaram o talento acadêmico.</p>
					<a href="<?php bloginfo('url'); ?>/deixe-sua-marca/fazendo-a-diferenca">Conheça</a>
				</div>
				
				<div id="embaixador-do-ismart" class="deixe-sua-marca">
					<h2><span>Embaixador<br> do</span> Ismart</h2>
					<p>Os professores que mais apoiam o Ismart podem se tornar embaixadores do projeto ao receber a <strong>"corujinha" de ouro</strong> nos Encontros Ismart com Educadores da Rede Pública.</p>
					<a href="<?php bloginfo('url'); ?>/deixe-sua-marca/embaixadores-do-ismart">Conheça</a>
				</div>

				<div id="galeria-de-mestres" class="deixe-sua-marca">
					<h2><span>Galeria de</span> Mestres Imortais</h2>
					<p>Os professores que já tiveram alunos<strong> aprovados no Processo Seletivo</strong> do Ismart merecem nosso reconhecimento.</p>
					<a href="<?php bloginfo('url'); ?>/deixe-sua-marca/mestres-imortais" style="display: none;">Conheça</a>
				</div>

				<div id="grupo-facebook" class="deixe-sua-marca">
					<h2><span>Grupo</span> Facebook</h2>
					<p>Saiba mais sobre o processo seletivo e<br> compartilhe dicas sobre como<br> encontrar jovens talentos nas escolas<br> públicas.</p>
					<a href="https://www.facebook.com/groups/234463936761508/" target="_blank">Conheça</a>
				</div>				
			</section>
		</section><!-- projetos -->
	
<?php get_footer(); ?>