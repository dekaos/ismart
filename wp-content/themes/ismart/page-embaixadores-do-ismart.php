<?php get_header(); ?>
    	<section id="feature" class="embaixadores">
	    	<h1><span>Embaixadores do</span> Ismart</h1>
	    	<p>Um reconhecimento especial aos educadores mais engajados com a causa do Ismart</p>
    	</section><!-- feature -->
    	
    	
    	<section id="projetos" class="c">
	    	<p>Os professores que mais apoiam o Ismart recebem o título de <strong>embaixadores no Encontro Ismart</strong> com Educadores da Rede Pública, evento anual que marca o início do processo seletivo para as bolsas de estudo do projeto. </p>

<p>O professor-embaixador tem os papeis de formador de opinião e de divulgador não só do processo seletivo, mas da instituição como um todo. Além de identificar alunos talentosos e prepará-los para concorrer às bolsas do Ismart, o embaixador apresenta o projeto aos demais professores, coordenadores e diretores da rede púbica de ensino. Ou seja, sua participação não fica restrita ao envolvimento da própria escola com o Ismart.
</p>
			<p><strong>Leia abaixo a história dos embaixadores do Ismart.</strong></p>
	    	<p class="divisor"></p>
			
			<section id="embaixadores" class="clearfix">
				<?php $args = array('post_type' => 'marca', 'tax_query' => array(array('taxonomy' => 'tipo', 'field' => 'slug', 'terms' => 'embaixadores'))); $embaixadores = new WP_Query($args); ?>
				<?php $num = 0; while($embaixadores->have_posts()) : $embaixadores->the_post(); $num++; $class = ($num % 2) ? ' even' : ' odd';  ?>

				<div class="embaixador <?php echo $class; ?>">
					<div class="imagem"><img src="<?php echo get_post_image_url('size_470-700'); ?>" width="470" height="700"></div>
					<div class="descricao"><h3><?php the_title(); ?></h3><?php the_content(); ?></div>
					<a href="<?php the_field('mar_link_blog'); ?>">Saiba mais</a>
				</div>
				<?php endwhile; ?>				
			</section>
		</section><!-- projetos -->
	
<?php get_footer(); ?>