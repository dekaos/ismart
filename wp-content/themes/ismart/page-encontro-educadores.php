<?php get_header(); ?>
    	<section id="feature" class="projetos encontro">
	    	<h1><span><strong>8º Encontro Ismart</strong> com<br>Educadores da Rede Pública</span></h1>
    	</section><!-- feature -->
    	
    	
    	<section id="projetos" class="c">
    		<img src="<?php echo get_template_directory_uri(); ?>/img/encontro-educadores.jpg">
	    	<div id="encontro" class="php_wysija clearfix" style="color: #FFF; width:820px; margin: 0 auto; background-color:#00b5e2; padding:0 100px 50px 100px;">
	    	    <h2 style="margin: 0 0 40px 0;">Escolha a cidade do evento</h2>
	    	    <div>
			<a hef="#" id="btn-sp">São Paulo</a>
			<a hef="#" id="btn-rj">Rio de Janeiro</a>
			</div>
			<div id="form-sp" style="display:none">
	    	    		<?php echo do_shortcode('[contact-form-7 id="724" title="Encontro São Paulo"]');
	    		?>
			</div>
			<div id="form-rj" style="display:none">
				<?php echo do_shortcode('[contact-form-7 id="726" title="Encontro Rio"]'); ?>
			</div>
	    	</div>
		<div class="clearfix">
		<a class="encontro2014" href="http://www.ismart.org.br/2014/05/ismart-reune-educadores-de-escolas-publicas-e-divulga-processo-seletivo/">Veja como foi o encontro de 2014</a>

		<a class="duvidas" href="http://www.ismart.org.br/#fale-conosco">Dúvidas? Fale Conosco!</a>

		</div>
	    	
							
	</section><!-- projetos -->
	
<?php get_footer(); ?>