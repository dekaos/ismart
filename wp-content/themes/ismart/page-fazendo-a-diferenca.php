<?php get_header(); ?>
    	<section id="feature" class="marca">
	    	<h1><span>Fazendo a</span> diferença</h1>
	    	<p>Histórias de professores que sabem identificar alunos com talento acadêmico</p>
    	</section><!-- feature -->
    	
    	
    	<section id="projetos" class="c">
	    	<p>Os educadores que tiveram <strong>alunos aprovados no processo seletivo do Ismart</strong> falam de sua relação com os estudantes e contam como identificaram o talento acadêmico.</p>
 
<p>A cada mês, neste espaço do programa "Deixe sua Marca", você encontrará a história de um professor que contribuiu para <strong>transformar a vida</strong> de um jovem talento ao indicá-lo para o processo seletivo do Ismart. 
</p>
			<p>A cada mês, neste espaço do Programa "Deixe sua Marca", você encontrará a história de um professor que contribuiu para transformar a vida de um jovem talento.</p>
	    	<p class="divisor"></p>
			
			<section id="diferenca" class="clearfix">
				<?php $args = array('post_type' => 'marca', 'tax_query' => array(array('taxonomy' => 'tipo', 'field' => 'slug', 'terms' => 'fazendo-a-diferanca'))); $diferenca = new WP_Query($args); ?>
				<?php while($diferenca->have_posts()) : $diferenca->the_post(); ?>

				<div class="diferencas clearfix">
					<div class="imagem"><img src="<?php echo get_post_image_url('size_460-400'); ?>" width="460" height="400"></div>
					<div class="descricao">
					<h1><?php the_title(); ?></h1>
						<?php the_content(); ?>
						<a href="<?php the_field('mar_link_blog'); ?>">Saiba mais</a>
					</div>			
				</div>
				<?php endwhile; ?>				
			</section>
		</section><!-- projetos -->
	
<?php get_footer(); ?>