<?php get_header(); ?>
<?php $site_url = get_site_url(); ?>
    	<section id="feature" class="projetos encontro" style="background: url(<?php echo $site_url; ?>/wp-content/uploads/2015/04/feature_guia.jpg) center; background-attachment: fixed;">
	    	<h1><span><strong>Guia Prático</strong><br>para a Seleção do Ismart</span></h1>
    	</section><!-- feature -->
    	
    	
    	<section id="projetos" class="c">
    		<img src="<?php echo $site_url; ?>/wp-content/uploads/2015/04/ismart-guia-pratico-icones-01.png">

<p class="azul">
O <strong>Guia Prático para a Seleção do Ismart</strong> tem o objetivo de<br>ajudar os educadores no processo seletivo do instituto.
<br/><br/>
Explica a importância do professor em cada etapa e mostra como<br>ele pode orientar seus alunos na busca por uma bolsa do Ismart.
	    	</p>
<br/>
<img src="<?php echo $site_url; ?>/wp-content/uploads/2015/04/desenho01.png">
<br/>
<br/><br/>
<br/>
<p class="azul">
Os educadores são os melhores “olheiros” do Ismart e ajudam<br>o instituto a encontrar os <strong>jovens talentos</strong> que merecem a oportunidade<br>de desenvolver ao máximo o seu potencial.
</p>

<p class="divisor"></p>
	
    	

<img src="<?php echo $site_url; ?>/wp-content/uploads/2015/04/ismart-guia-pratico-icones-04.png">
<br/>
<h1 class="azul">Inscrição</h1>
<p style="width:640px; margin: 0 auto;">
O olhar atento do educador é essencial para identificar alunos com o <strong>potencial de se tornarem bolsistas</strong> do Ismart. 
<br><br>
O professor, o coordenador pedagógico e o diretor escolar acompanham os estudantes diariamente e podem <strong>ampliar as oportunidades</strong> de desenvolvimento de jovens talentos da rede pública de ensino inscrevendo-os no processo seletivo do Ismart.<br><br>
</p>
<h2 style="color:#fcb040;">Fique atento</h2>
<p style="width:640px; margin: 0 auto; color:#00b5e2;">
As inscrições para o processo seletivo são abertas anualmente, em <strong>abril ou maio</strong>. Os educadores podem inscrever os candidatos no site <strong>www.ismart.org.br</strong>, onde também encontram o regulamento da seleção. 
<br><br>
Não deixe seus alunos talentosos perderem essa chance!
</p>
<p class="divisor"></p>



<img src="<?php echo $site_url; ?>/wp-content/uploads/2015/04/ismart-guia-pratico-icones-05.png">
<br/>
<h1 class="azul">Teste Online</h1>
<p style="width:640px; margin: 0 auto;">
A ajuda do educador vai além da inscrição do aluno no processo seletivo do Ismart. O cadastro só está completo quando o candidato realiza o Teste Online, com <strong>questões objetivas de português e matemática</strong>. É importante que o educador tenha certeza que os alunos que indicou <strong>completaram essa etapa até o fim</strong>.
<br><br>
</p>
<h2 style="color:#fcb040;">Você sabia?</h2>
<p style="width:640px; margin: 0 auto; color:#00b5e2;">
Em média, 20% dos alunos inscritos são desclassificados do processo seletivo por não realizarem o Teste Online. Informe seus alunos da necessidade de responder às questões até <strong>7 dias</strong> após o preenchimento dos dados no sistema e aumente a chance de eles se tornarem bolsistas do Ismart! 
</p>
<p class="divisor"></p>



<img src="<?php echo $site_url; ?>/wp-content/uploads/2015/04/ismart-guia-pratico-icones-06.png">
<br/>
<h1 class="azul">Prova Presencial</h1>
<p style="width:640px; margin: 0 auto;">
A Prova Presencial é a segunda fase do processo seletivo do Ismart, e o educador pode ajudar os alunos a participar desta etapa com tranquilidade. É importante que eles mantenham a calma: muitos devem estar participando de uma seleção pela primeira vez. Explique a importância de chegar com antecedência ao local da prova, levando o comprovante de classificação impresso.
<br><br>
</p>
<h2 style="color:#fcb040;">Uma dica</h2>
<p style="width:640px; margin: 0 auto; color:#00b5e2;">
No site do Ismart há <strong>exemplos de questões de provas de anos anteriores</strong>. Os educadores podem fornecer o material aos alunos, para estudo prévio. Algumas escolas fazem ainda plantões de dúvidas e criam listas de exercícios de apoio.
</p>
<p class="divisor"></p>



<img src="<?php echo $site_url; ?>/wp-content/uploads/2015/04/ismart-guia-pratico-icones-07.png">
<br/>
<h1 class="azul">Entrevista Individual</h1>
<p style="width:640px; margin: 0 auto;">
A lista de classificados na Prova Presencial é publicada no site do Ismart. Após a divulgação dos resultados, os aprovados seguem para a etapa de Entrevista Individual. A missão do educador, então, será esclarecer dúvidas dos candidatos sobre o comportamento em entrevistas. Conte para eles um pouco de sua experiência nos processos em que participou!
<br><br>
</p>
<h2 style="color:#fcb040;">É bom saber</h2>
<p style="width:640px; margin: 0 auto; color:#00b5e2;">
Uma excelente estratégia pode ser a <strong>promoção de trocas de experiências com alunos</strong> que já passaram pelo processo seletivo. Sua escola tem ex-alunos que hoje são bolsistas do Ismart? Chame-os para um bate-papo! Nós podemos te ajudar com esta tarefa.
</p>
<p class="divisor"></p>



<img src="<?php echo $site_url; ?>/wp-content/uploads/2015/04/ismart-guia-pratico-icones-08.png">
<br/>
<h1 class="azul">Visita Domiciliar</h1>
<p style="width:640px; margin: 0 auto;">
Na Visita Domiciliar, o Ismart conhece a família do candidato. Um profissional da nossa equipe vai à casa do aluno para compreender, entre outras coisas, <strong>a importância que a família dá aos estudos</strong>. Quanto mais membros da família estiverem presentes, além dos responsáveis, melhor!
<br><br>
</p>
<h2 style="color:#fcb040;">Fique por dentro</h2>
<p style="width:640px; margin: 0 auto; color:#00b5e2;">
Você já sabe quem são os responsáveis pelo aluno que indicou ao processo seletivo? Esta pode ser uma oportunidade de conhecê-los, convocando-os para uma reunião na escola. Para tranquilizar a família, vale informar sobre o trabalho do Ismart, as etapas da seleção e, acima de tudo, lembrar que os candidatos já chegaram muito longe!
</p>
<p class="divisor"></p>



<img src="<?php echo $site_url; ?>/wp-content/uploads/2015/04/ismart-guia-pratico-icones-09.png">
<br/>
<h1 class="azul">Dinâmica de Grupo</h1>
<p style="width:640px; margin: 0 auto;">
A Dinâmica de Grupo é a última fase do processo seletivo. Nesta etapa, o candidato realiza atividades em conjunto com outros alunos para que sejam avaliados alguns elementos como criatividade, habilidade de comunicação e socialização e, sobretudo, a motivação de cada candidato em se tornar um bolsista do Ismart.
<br><br>
</p>
<h2 style="color:#fcb040;">Mantenha a calma</h2>
<p style="width:640px; margin: 0 auto; color:#00b5e2;">
Além do auxílio do educador na preparação dos alunos para a Dinâmica de Grupo, <strong>é hora de ajudá-los a ter paciência</strong>. Os candidatos ficarão ansiosos pela divulgação dos resultados finais do processo seletivo do Ismart. Tranquilize-os mostrando que eles já chegaram à última fase, um grande feito!
</p>
<p class="divisor"></p>



<img src="<?php echo $site_url; ?>/wp-content/uploads/2015/04/ismart-guia-pratico-icones-10.png">
<br/>
<h1 class="azul">Resultado</h1>
<p style="width:640px; margin: 0 auto;">
Depois de encerrada a fase da Dinâmica de Grupo, os candidatos podem consultar os resultados através do site www.ismart.org.br. Aproveite este momento para <strong>celebrar com os aprovados</strong> e oferecer suporte aos não aprovados, apresentando-lhes outras oportunidades de desenvolvimento disponíveis.
<br><br>
</p>
<h2 style="color:#fcb040;">Olho vivo!</h2>
<p style="width:640px; margin: 0 auto; color:#00b5e2;">
É importante conscientizar seu aluno de que, caso não seja aprovado, as chances dele não se esgotam aí! Se o estudante concorreu ao Projeto Alicerce, no 9º ano poderá se inscrever para o Bolsa Talento. Se já está no 9º ano, explique que há muitas escolas particulares que também oferecem programas de bolsa.
</p>
<p class="divisor"></p>


<!--
		<div class="clearfix">
		<a class="encontro2014" href="http://www.ismart.org.br/2014/05/ismart-reune-educadores-de-escolas-publicas-e-divulga-processo-seletivo/">Veja como foi o encontro de 2014</a>

		<a class="duvidas" href="http://www.ismart.org.br/#fale-conosco">Dúvidas? Fale Conosco!</a>

		</div>
	    	-->
							
	</section><!-- projetos -->
	
<?php get_footer(); ?>