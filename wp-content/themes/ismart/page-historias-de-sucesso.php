<?php get_header(); ?>
    	<section id="feature" class="historias">
	    	<h1><span>Histórias de</span> Sucesso</h1>
	    	<p>Transformar vidas é um dos principais resultados do ISMART.</p>
    	</section><!-- feature -->
    	
    	
    	<section id="projetos" class="c">
	    	<p>Ao identificar e apoiar jovens talentos das escolas da rede pública, <strong>a instituição<br>contribui para que esses alunos possam exercer a cidadania.</strong></p> 
			<p>Que tal conhecer de perto as histórias de sucesso de estudantes que despertam<br>seu potencial para construir uma nova realidade para si e, por que não, para o país?</p>
	    	
	    	</p>
	    	<p class="divisor"></p>		
			<section id="historias-container" class="clearfix">
				<?php $args = array('post_type' => 'historia'); $historia = new WP_Query($args); ?>
				<?php $num = 0; while($historia->have_posts()) : $historia->the_post(); $num++; $class = ($num % 2) ? ' even' : ' odd';  ?>
				<?php $cat = get_category(get_field('his_projeto')); ?>
				<div class="historia <?php echo $cat->slug . ' ' . $class; ?> clearfix">
					<div class="historia-imagem">
						<img src="<?php echo get_post_image_url('size_460-400'); ?>" width="470" height="400">
					</div>
					<div class="historia-descricao">
						<h1><?php the_title(); ?></h1>
						<?php the_content(); ?>
						<?php if(get_field('his_link_blog')) : ?><a href="<?php the_field('his_link_blog'); ?>" class="saiba-mais">Saiba mais</a><?php endif; ?>
					</div>
				</div>
				<?php endwhile; ?>
			</section>
		</section><!-- projetos -->
<?php get_footer(); ?>