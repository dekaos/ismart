<?php get_header(); ?>
    	<section id="feature" class="projetos ismart-online">
	    	<h1><span>Projeto</span> Ismart Online</h1>
	    	<p><strong>Projeto realizado em caráter experimental pelo Ismart com alunos do 8º ano do ensino fundamental</strong> de São Paulo, São José dos Campos e Rio de Janeiro.</p>
    	</section><!-- feature -->
    	
    	
    	<section id="projetos" class="c">
	    	<p class="vermelho">Os estudantes têm acesso a uma <span>plataforma de ensino online<br> com conteúdos de português e matemática e a uma<br> "Módulo de Cultura Ismart"</span>, que se propõe a desenvolver<br> habilidades como motivação, autonomia, persistência e<br> inspiração.</p>
	    	<p class="divisor vermelho"></p>
			<p>Os alunos continuam matriculados em suas escolas de origem e receberam uma <strong>licença de acesso ao<br> Ismart Online porque chegaram muito perto de entrar para o Projeto Alicerce em 2014.</strong></p>
			
			<section id="projeto-container" class="clearfix">
			
					<div id="modulo-pt-mat">
						<h2>Módulo de <strong>Português<br>e Matemática</strong></h2>
						<a href="https://www.geekielab.com.br/login/" class="saiba-mais" target="_blank">Veja aqui</a>
					</div>
					
					<div id="modulo-cultura">
						<h2>Módulo de<br><strong>Cultura Ismart</strong></h2>
						<a style="float:left; margin: 0 30px 0 50px;" href="http://vimeopro.com/user25854406/modulo-de-cultura-8-ano" class="saiba-mais" target="_blank">8º ano</a> <a style="float:left;" href="http://vimeopro.com/user25854406/modulo-de-cultura-9-ano" class="saiba-mais" target="_blank">9º ano</a>
					</div>
					
					<?php if(have_rows('rf_escolas_parceiras')) : ?>
					<section id="escolas-parceiras" class="clearfix">
						<h1><span>Escolas Parceiras do</span> Projeto Alicerce</h1>
						<?php while(have_rows('rf_escolas_parceiras')) : the_row(); ?>
						<ul class="escolas clearfix">							
							<h2><?php the_sub_field('es_cidade'); ?></h2>
							<?php while(have_rows('rf_escolas')) : the_row(); ?>
							<li><?php if(get_sub_field('es_link')) : ?><a href="<?php the_sub_field('es_link'); ?>"> <?php endif; ?><img src="<?php echo resize_image(get_sub_field('es_imagem'), '175x70'); ?>" alt="<?php the_title(); ?>" width="175" height="70"><?php if(get_sub_field('es_link')) : ?></a><?php endif; ?></li>
							<?php endwhile; ?>
						</ul>
						<?php endwhile; ?>
					</section>
					<?php endif; ?>
			</section>
		</section><!-- projetos -->
	
<?php get_footer(); ?>