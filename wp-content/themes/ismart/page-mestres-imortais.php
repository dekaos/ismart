<?php get_header(); ?>
    	<section id="feature" class="mestres">
	    	<h1><span>Galeria de</span> Mestres Imortais</h1>
	    	<p>Professores que deixaram sua marca eternamente na vida de alunos talentosos</p>
    	</section><!-- feature -->
    	
    	
    	<section id="projetos" class="c">
	    	<p>Conheça todos os educadores que tiveram estudantes aprovados no processo seletivo do Ismart.</strong>. 
</p>
			<p>Passe o mouse sobre o rosto para identificar o professor, a escola onde ensina e o nome do aluno bolsista.</p>
	    	<p class="divisor"></p>
			
			<section id="mestres" class="clearfix">
			<ul class="anos">
				<?php $terms = get_terms('ano', 'hide_empty=1&order=DESC'); $num = 0; foreach($terms as $term) { $num++; $class = ($num == 1) ? ' ativo' : ''; ?>
	    		<li class="<?php echo $class; ?>"><a href="#" data-ano="<?php echo $term->name; ?>"><?php echo $term->name; ?></a></li>
	    		<?php } ?>
	    	</ul>
			
			<?php $terms = get_terms('ano', 'hide_empty=1&order=DESC'); $num = 0; foreach($terms as $term) { $num++; $class = ($num == 1) ? ' style="display:block;"' : ''; ?>
	    	<ul class="ano <?php echo $term->name; ?> clearfix" <?php echo $class; ?>>
				<?php $args = array('post_type' => 'marca', 'tax_query' => array('relation' => 'AND', array('taxonomy' => 'tipo', 'field' => 'slug', 'terms' => 'mestres-imortais'), array('taxonomy' => 'ano', 'field' => 'slug', 'terms' => $term->slug))); $diferenca = new WP_Query($args); ?>
				<?php while($diferenca->have_posts()) : $diferenca->the_post(); ?>
		    	<li>
		    		<img src="<?php echo get_post_image_url('size_220-220'); ?>" width="220" height="220">
		    		<div class="texto">
			    		<?php the_content(); ?>
		    		</div>
		    	</li>
		    	<?php endwhile; ?>		    			    			    	
	    	</ul>
	    	<?php } ?>	    	
			</section>
		</section><!-- projetos -->
	
<?php get_footer(); ?>