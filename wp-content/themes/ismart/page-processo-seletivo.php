<?php get_header(); ?>
		<section id="header-processo-seletivo" style="background: url(http://www.ismart.org.br/wp-content/uploads/2014/05/feature_processo.jpg) no-repeat center center;">
			<div class="c clearfix">
				<div class="col1">
					<h1><span class="processo">Processo</span><br>Seletivo<br><span class="ano">2015</span></h1>
				</div>
				
				<div class="balao">
					<h2 style="margin: 110px 0 -10px 190px;">As inscrições para o processo seletivo vão<br><strong>até 12 de junho!</strong></h2>
					<p>Se você gosta de aprender e quer transformar a sua própria história, você<br>precisa conhecer o Ismart!</p>
				</div>
				
				<div class="icones">
					<h2><strong>As inscrições são gratuitas</strong> e<br> podem ser feitas pelo professor ou<br> pelo próprio aluno</h2>
				</div>

			</div>

		</section>
    	<!-- <section id="projetos" class="c" style="padding: 0px 0 60px 0;">
    	<a href="http://www.ismart.org.br/encontro-educadores"><img src="http://www.ismart.org.br/wp-content/uploads/2015/03/bt_encontro.png" width="411" height="240"></a>
</section>
-->


    	<section id="projetos" class="c" style="padding: 0px 0 60px 0;">
	    	<p>Para concorrer a uma bolsa de estudo do Ismart, o aluno não pode ter repetido o ano escolar<br>e deve vir de família com renda por pessoa de no máximo dois salários mínimos.</p>
			
			<a href="http://primeiraescolha.com.br/ismart/" target="_blank" class="inscreva-se big">Inscreva-se aqui</a>
			<!-- <a href="http://www.ismart.org.br/wp-content/uploads/2014/10/Ismart_Declacao_Autonomo.pdf" target="_blank" class="inscreva-se big">Declaração de Autônomo</a> -->
			<section id="inscricoes" class="clearfix">
				<h1><span>As inscrições podem ser feitas</span><br>pelo professor ou pelo próprio aluno.</h1>
				<div class="projeto-alicerce">
					<p style="color:#00b5e2;">Os estudantes matriculados no 7º ano do ensino fundamental concorrem ao <strong>Projeto Alicerce</strong>.</p>
					<a style="color:#00b5e2;" href="http://www.ismart.org.br/?page_id=97">Saiba mais</a>
					<a style="color:#00b5e2;" href="http://www.ismart.org.br/wp-content/uploads/2015/04/Regulamento_Processo_Seletivo_Alicerce_2015.pdf" target="_blank">Regulamento</a>				
				</div>
				
				<div class="projeto-bolsa-talento">
					<p>Os estudantes matriculados no 9º ano do ensino fundamental concorrem ao <strong>Projeto Bolsa Talento</strong>.</p>
					<a href="http://www.ismart.org.br/?page_id=108">Saiba mais</a>
					<a href="http://www.ismart.org.br/wp-content/uploads/2015/04/Regulamento_Processo_Seletivo_Bolsa_Talento_2015.pdf" target="_blank">Regulamento</a>
				</div>
				<div class="professores">
					<h1><span>Professores</span></h1>
					<p>Acesse o Guia Prático<br>para a Seleção do Ismart.</p>
					<a href="http://www.ismart.org.br/guia-educadores" target="_blank">Acessar</a>
				</div>
				<div class="estudantes">
					<h1><span>Estudantes</span></h1>
					<p>Veja as principais informações<br>do processo seletivo no Guia do Aluno.</p>
					<a href="http://www.agenciaideal.com.br/ismart/guiadoaluno/index.html" target="_blank">Guia do Aluno</a>
					<a href="../wp-content/uploads/2014/05/exemplos-questoes-anteriores.pdf" target="_blank" class="anteriores">Questões de anos anteriores</a>
				</div>			
			</section>		
		</section> <!-- projetos -->
		
		<section id="etapas">
			<div class="c">
				<h1>O processo seletivo é composto de <strong>cinco etapas</strong>,<br> todas eliminatórias:</h1>
				<ul class="clearfix">
					<li>
						<span class="numero">1</span>
						Teste<br>Online
					</li>
					<li>
						<span class="numero">2</span>
						Prova<br>Presencial
					</li>
					<li>
						<span class="numero">3</span>
						Entrevista<br>Individual
					</li>
					<li>
						<span class="numero">4</span>
						Visita<br>Domiciliar
					</li>
					<li>
						<span class="numero">5</span>
						Dinâmica<br>de Grupo
					</li>										
				</ul>
				<h2>O resultado de cada fase da seleção <strong>é publicado no site do Ismart.</strong></h2>
			</div>

		</section>
		

		<section id="perguntas-frequentes">
		<h1><span>Perguntas</span> Frequentes</h1>
			<dl class="c">
				<?php $args = array('post_type' => 'pergunta', 'orderby' => 'date', 'order' => 'ASC'); $pergunta = new WP_Query($args); ?>
				<?php while($pergunta->have_posts()) : $pergunta->the_post(); ?>
				<dt class="closed"><a href="#" class="title r setadown"><?php the_title(); ?></a></dt>
				<dd class="content"><?php the_content(); ?></dd>
				<?php endwhile; ?>				
			</dl>					
		</section>
	
<?php get_footer(); ?>