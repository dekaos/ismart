<?php get_header(); ?>
    	<section id="feature" class="projetos">
	    	<h1><span>Projetos</span></h1>
	    	<p>O Ismart foi criado para apoiar o desenvolvimento de<br> <strong>jovens talentos acadêmicos</strong> identificados em um<br>processo seletivo anual.</p>
    	</section><!-- feature -->
    	
    	
    	<section id="projetos" class="c">
	    	<p class="azul">O instituto busca alunos com <span>excelentes notas escolares</span>, que gostam de aprender e apresentam motivação para superar desafios.
<!--
	<p class="azul">As inscrições foram prorrogadas!</p><br><br>
Para o <span>Bolsa Talento</span> (candidatos do 9º. ano do Ensino Fundamental) as inscrições e conclusão do teste online estarão abertas até <span>27 de julho</span>.<br><br>
Para o <span>Projeto Alicerce</span> (candidatos do 7º. ano), o prazo limite para inscrições e conclusão do teste online é <span>06 de agosto</span>.
<br><br>Não perca essa oportunidade!</p>
-->
	    	<p class="divisor"></p>
	    	<p>Os estudantes aprovados no processo seletivo poderão receber, gratuitamente, até<br> três bolsas, uma após a outra.</p>
		
			<section id="projeto-container" class="clearfix">
				<div class="projeto projeto-bolsa-talento projeto-v">
					<h1><span>Projeto</span><br>Bolsa Talento</h1>
					<p>Os alunos que recebem a primeira bolsa e, durante o curso preparatório, conseguem bom desempenho, têm boa disciplina e são aprovados nos exames de seleção das escolas particulares, conseguem uma nova bolsa para o ensino médio.</p>
					<p>Os estudantes aprovados no processo seletivo para o Projeto Bolsa Talento entram diretamente no ensino médio.</p> 
					<p>O Ismart concede bolsas para o ensino médio em São Paulo (capital, Cotia, São José dos Campos e Sorocaba) e no Rio de Janeiro.</p>
					<a href="<?php bloginfo('url'); ?>/?page_id=108">Saiba mais</a>
				</div>
				<div class="projeto projeto-alicerce projeto-h">
					<h1><span>Projeto</span> Alicerce</h1>
					<p>Os alunos aprovados para o Projeto Alicerce participam de um curso preparatório de dois anos de duração para as provas de seleção para o ensino médio nas melhores escolas particulares de São Paulo, São José dos Campos e Rio de Janeiro.</p>
					<a href="<?php bloginfo('url'); ?>/?page_id=97">Saiba mais</a>
				</div>
				<div class="projeto projeto-bolsa-universitarios projeto-h">
					<h1><span>Projeto</span> Bolsa Universitários</h1>
					<p>Os alunos que durante os três anos do ensino médio alcançam média geral acima de 7,0, têm boa disciplina e são aprovados nos vestibulares para cursos e instituições apoiadas pelo Ismart recebem bolsa-auxílio para cursar a faculdade.</p>
					<a href="<?php bloginfo('url'); ?>/?page_id=104">Saiba mais</a>
				</div>
				<div class="projeto projeto-ismart-online projeto-h2">
					<h1><span>Projeto</span> Ismart Online</h1>
					<p>Os estudantes têm acesso a uma plataforma de ensino online com conteúdos de português e matemática e a uma "Módulo de Cultura Ismart", que se propõe a desenvolver habilidades como motivação, autonomia, persistência e inspiração.</p>
					<a href="<?php bloginfo('url'); ?>/?page_id=106">Saiba mais</a>
				</div>
				
			</section>
		</section><!-- projetos -->
		
		<section id="header-processo-seletivo" style="background: url(http://www.ismart.org.br/wp-content/uploads/2014/05/feature_processo.jpg) no-repeat center center;">
			<div class="c clearfix">
				<div class="col1">
					<h1><span class="processo">Processo</span><br>Seletivo<br><span class="ano">2015</span></h1>
				</div>
				<div class="col2">
					<a href="<?php bloginfo('url'); ?>/?page_id=133" class="inscreva-se">Inscreva-se Aqui</a>
				</div>
				
				<div class="balao">
					<h2>As inscrições para o processo seletivo vão até 12 de junho!</h2>
					<p>Se você gosta de aprender e quer transformar a sua própria história, você precisa conhecer o Ismart!</p>
				</div>
				
				<div class="icones">
					<h2><strong>As inscrições são gratuitas</strong> e<br> podem ser feitas pelo professor ou<br> pelo próprio aluno</h2>
				</div>
			</div>
		</section>

<?php get_footer(); ?>