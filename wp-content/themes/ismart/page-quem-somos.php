<?php get_header(); ?>
    	<section id="feature" class="quem-somos">
	    	<h1><span>Quem</span> Somos</h1>
	    	<p>Criado em 1999, o <strong>Instituto Social para Motivar, Apoiar e Reconhecer Talentos (Ismart)</strong> é uma entidade privada, sem fins lucrativos, que identifica jovens talentos de baixa renda, de 12 a 14 anos de idade, e lhes concede <strong>bolsas em escolas particulares de excelência</strong> e o acesso a programas de desenvolvimento e orientação profissional, do ensino fundamental à universidade.</p>
    	</section><!-- feature -->
    	
    	
    	<section id="quem-somos" class="c">
	    	<p class="azul">Atualmente o instituto tem cerca de <strong>1.000 bolsistas ativos</strong> em São Paulo<br>(capital, Cotia, São José dos Campos e Sorocaba) e no Rio de Janeiro.</p>
	    	<p class="divisor"></p>
	    	<p>A atuação do Ismart está pautada na convicção de que jovens talentos podem ser encontrados<br>em todas as camadas da população, independentemente de faixa de renda, origem étnica ou<br> social. A instituição acredita que, <strong>com acesso a educação de qualidade, os bolsistas podem<br> sonhar mais alto e atingir o sucesso profissional.</strong></p>
	    	<p>Assim, o Ismart espera contribuir para mudar a composição da futura elite intelectual brasileira,<br> garantindo que seus líderes reflitam a verdadeira face do país.</p>
    	</section><!-- quem-somos -->
    	
    	<section id="missao-visao-valores">
	    	<ul>
	    		<li class="missao ativo"><a href="#">Missão</a></li>
	    		<li class="visao"><a href="#">Visão</a></li>
	    		<li class="valores"><a href="#">Valores</a></li>
	    	</ul>
	    	<section id="missao"><p>Concretizar o pleno potencial profissional de jovens talentos acadêmicos de baixa renda através de programas calcados na valorização da excelência, da ética e da criatividade produtiva.</p></section><!-- missao -->
	    	<section id="visao"><p>Formar e colocar no mercado de trabalho, até 2020, 250 bolsistas do Ismart nas carreiras apoiadas e em posições de destaque.</p></section><!-- visao -->
	    	<section id="valores"><p>
	    	• As diferentes formas de talento se distribuem por todas as classes sociais.<br>
			• O talento só se concretiza por meio do esforço pessoal.<br>
			• Os bons exemplos têm efeito multiplicador.<br>
			• As boas oportunidades catalisam a promoção social.<br>
			• A excelência é a base para os resultados desejados.<br>
			• A definição de sucesso é o resultado de uma atividade produtiva, da responsabilidade social e da realização pessoal.</p></section><!-- valores -->
    	</section><!-- missao-visao-valores -->
    	<?php $args = array('post_type' => 'relatorio', 'posts_per_page' => -1, 'order' => 'DESC'); $relatorio = new WP_Query($args); ?>
    	<?php if($relatorio->have_posts()) : ?>
    	<section id="relatorios-atividade">
	    	<h1><span>Relatórios de</span> Atividade</h1>
	    	<ul class="c clearfix">
		    	<?php while($relatorio->have_posts()) : $relatorio->the_post(); ?>
		    	<li>
			    	<p><?php the_title(); ?></p>
			    	<a href="<?php the_field('rel_arquivo'); ?>" target="_blank">
				    	<img src="<?php echo get_post_image_url('size_170-254'); ?>" alt="<?php the_title(); ?>" width="170" height="254">
			    	</a>
		    	</li>
		    	<?php endwhile; ?>
	    	</ul>
    	</section><!-- relatorios-atividade -->
    	<?php endif; ?>
<?php get_footer(); ?>