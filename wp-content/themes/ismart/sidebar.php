		<aside id="sidebar">
			<section id="search">
			   <form action="<?php echo get_bloginfo('url'); ?>/" method="get" accept-charset="utf-8" id="searchform" role="search" class="clearfix">
			   		<input type="text" name="s" id="s" class="default-value" value="Busca">
			   		<button type="submit" value="">
			   			<i class="fa fa-search"></i>
			   		</button>
			   </form>
			</section>
			
			<section id="categorias" class="sidebar-item">
				<h1>Categorias</h1>
				<ul>				
				<?php wp_list_categories(array('orderby' => 'count', 'title_li' => 0, 'exclude' => 8)); ?>
				</ul>
			</section>
				
			<section id="ultimas-noticias" class="sidebar-item">
				<h1>Últimas Notícias</h1>
				<ul>
					<?php $args = array('post_type' => 'post', 'posts_per_page' => 4, 'category__not_in' => array(8)); $blog = new WP_Query($args); while($blog->have_posts()) : $blog->the_post();  ?>
					<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
					<?php endwhile; ?>
				</ul>
			</section>
			
			<section id="facebook-widget" class="sidebar-item">
				<h1>Facebook</h1>
				<div class="fb-like-box" data-href="https://www.facebook.com/ismart.oficial" data-width="290" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false"></div>
			</section>
			
			<section id="nuvem-tags" class="sidebar-item">
				<h1>Nuvem de Tags</h1>
				<?php wp_tag_cloud(array('smallest' => '12', 'largest' => '18', 'unit' => 'px')); ?>
			</section>
			
		</aside>
