<?php get_header(); ?>
		<?php if(have_posts()) : the_post();  ?>
    	<section id="feature" class="blog single" style="background:url(<?php echo get_post_image_url('full'); ?>) center center no-repeat; background-size: cover; background-attachment: fixed;">
	    	<h1><span>BLOG</span></h1>
    	</section><!-- feature -->
		<section class="c clearfix">
		<section id="blog-content">
			<article class="post">
				<header><span class="data"><?php the_time('d F Y'); ?></span> • <span class="categoria"><?php $categories = get_the_category(); $separator = ', '; $output = ''; if($categories){ foreach($categories as $category) { $output .= '<a href="'.get_category_link( $category->term_id ).'" title="' . esc_attr( sprintf( __( "View all posts in %s" ), $category->name ) ) . '">'.$category->cat_name.'</a>'.$separator; } echo trim($output, $separator); } ?></span></header>
				<h1><?php the_title(); ?></h1>
				<?php if(has_post_thumbnail()) : ?>
				<img src="<?php echo get_post_image_url('size_560-360'); ?>">
				<?php endif; ?>
				<?php the_content(); ?>
				<div id="shareme" class="clearfix" data-url="<?php the_permalink();?>" data-text="<?php echo get_the_excerpt(); ?>"></div>
				<div class="fb-comments" data-href="<?php the_permalink(); ?>" data-width="560" data-numposts="10" data-colorscheme="light"></div>	
			</article>
		</section>
		
		<?php get_sidebar(); ?>		
		
		</section>
		<?php endif; ?>
<?php get_footer(); ?>